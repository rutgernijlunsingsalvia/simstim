# SimStim

Simulate Your Own Stimulator!

## Context

A medical device company wants to build a neuro-stimulator. The neuro-stimulator has (only) 2 contacts: one is alwys anodic, and the other one is always cathodic. The stimulator will output a stream of pulses, as can be seen in the graph below of the current (I) over time (t):

![alt text](Pulse Shape.png "Shape of the pulse")

Positive current represents the charge phase, and negative current represents the discharge phase.

The parameters (`struct StimParams`) describing the pulse are:
- `frequencyInHz`: The frequency (4 to 130Hz) with which the pulse should start again. Range: 4 to 130Hz
-	`amplitudeInMicroamps`: The amplitude (0 to 10mA) of the charge pulse. Range: 0 to 10mA
- `chargePulseWidthInMicroseconds`: The duration. Range: 50us to 1ms
- `dischargeAmplitudePercentage`: The amplitude of the discharge pulse, expressed relative to charge amplitude. Range: 1% to 100%

## Requirements
  
| ID | Requirement |
| ------ | ------ |
| REQ1 | The programming language is plain C |
| REQ2 | One pulse consist of a charge phase and a discharge phase |
| REQ3 | [SAFETY] The pulses must be charged-balanced: the charge and discharge area must be the same |
| REQ4 | The target MCU is a cheap 32-bits without FPU and MPU |


## Task

We want to sample the graph with a fixed frequency `sampleFreqInHz`, and sequentially get the required amplitude for all these samples. Therefore 2 C functions need to be implemented:

-	`init(int sampleFreqInHz, struct StimParams *stimParams)`
  This function is called once before any sample creation. It can be used to perform additional initialization.
-	`nextSample(int sampleFreqInHz, struct StimParams *stimParams)`
  This function is called for every sample, and must return the current amplitude in microamps.
  This function Will be called by the main program with a frequency of `sampleFreqInHz` Hz.
  It must return the (signed) `amplitudeInMicroamps`.
  Plotting the output of `nextSample()` should result in the graph given.

The file `main.c` in this archive contains the skeleton code to fill in.

## Hints

 - Physical embedded harware is not required.
 - This is an opportunity for you to show your [software engineering](https://en.wikipedia.org/wiki/Software_engineering) skills.
 - Develop using the agile methodology.
 - Please try to keep it simple.
 - We expect you to spend no more than 2 hours of your time, so don't feel you need to complete it. Good luck!
 - Compilation is not mandatory, but might help you.

## Test

For example, the input:

```c
struct StimParams p = {
    .frequencyInHz = 1000,
    .amplitudeInMicroamps = 45,
    .dischargeAmplitudePercentage = 25,
    .chargePulseWidthInMicroseconds = 100
};
int sampleFreqInHz = 10000;

init(sampleFreqInHz, &p);
for (int i = 0; i < 20; i++) {
    printf("%i ", nextSample(sampleFreqInHz, &p));
}
```

...has as expected output:
```
45 -11 -11 -11 -11 -1 0 0 0 0 45 -11 -11 -11 -11 -1 0 0 0 0
```


