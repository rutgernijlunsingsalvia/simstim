struct StimParams {
	int frequencyInHz;                     // 4 .. 130Hz
	int amplitudeInMicroamps;              // 0 .. 10000uA
	int chargePulseWidthInMicroseconds;    // 50 .. 1000us
	int dischargeAmplitudePercentage;      // 1 .. 100%
}

// sampleFreqInHz: 1e5 .. 10e6
void init(int sampleFreqInHz, struct StimParams* stimParams) {
	// ... your code goes here ...
}

// Returns �amplitude in microamps� for this sample
int nextSample(int sampleFreqInHz, struct StimParams* stimParams) {
	// ... your code goes here ...
}
